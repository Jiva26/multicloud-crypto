package hr.fer.zemris.demo;

import hr.fer.zemris.sqlanalyzer.SqlAnalyzer;

public class Demo {

    public static void main(String[] args) {
        SqlAnalyzer analyzer = new SqlAnalyzer();
        analyzer.run();
    }
}
