package hr.fer.zemris.sqlanalyzer;

import hr.fer.zemris.input.Column;
import hr.fer.zemris.input.DBModel;
import hr.fer.zemris.input.Table;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.arithmetic.*;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.view.CreateView;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.statement.update.Update;

import java.io.*;
import java.util.*;

public class SqlAnalyzer {

    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";

    private static final int NUMBER_OF_REQUESTS = 4;

    private int[] resultStatistics = new int[5];
    private static String[] resultsMeaning = { "split", "split, ENC(D)", "split, ENC(D), ENC(D+seed)", "split, ENC(D), HASH(D)", "split, ENC(D), ENC(D+seed), HASH(D)"};

    private List<String> listOfTables = new ArrayList<>();

    private ExpressionVisitor conditionVisitor;

    /**
     * Map containing names of all data in database and list for counting requests.
     * String name of data
     * List<Integer> counter for certain data
     */
    private Map<String, int[]> requestCounter;
    private ArrayList<String> queries;
    private DBModel database;

    /**
     * Function that initialize database data.
     * @throws IOException
     */
    public void initializeDatabase() throws IOException {
        this.database = new DBModel();
        BufferedReader br = new BufferedReader(new FileReader("src\\main\\resources\\dbmodel.txt"));
        String line;
        while ((line = br.readLine()) != null) {
            String[] data = line.split(",");
            Table table = new Table(data[0]);
            for (int i = 1; i < data.length; i++) {
                table.addColumn(new Column(data[i]));
            }
            database.addTable(table);
        }
    }

    /**
     * Function that runs analyzator.
     */
    public void run() {
        try {
            initializeDatabase();
        } catch (FileNotFoundException e) {
            System.out.println("DB File not found.");
        } catch (IOException e) {
            System.out.println("Error reading DB file.");
        }

        initializeRequestCounter();

        instanceConditionVisitor();

        readSQLQueries();
    }

    /**
     * Function that instances conditional visitor.
     */
    private void instanceConditionVisitor() {

        conditionVisitor = new ExpressionVisitorAdapter() {

            @Override
            public void visit(AndExpression expr) {
                expr.getLeftExpression().accept(this);
                expr.getRightExpression().accept(this);
            }

            @Override
            public void visit(OrExpression expr) {
                expr.getLeftExpression().accept(this);
                expr.getRightExpression().accept(this);
            }

            @Override
            public void visit(Between expr) {
                expr.getLeftExpression().accept(new ExpressionVisitorExtended(2));
                expr.getBetweenExpressionStart().accept(new ExpressionVisitorExtended(2));
                expr.getBetweenExpressionEnd().accept(new ExpressionVisitorExtended(2));
            }

            @Override
            public void visit(InExpression expr) {
                expr.getLeftExpression().accept(new ExpressionVisitorExtended(1));
                if (expr.getRightItemsList() instanceof SubSelect) {
                    try {
                        analyzeQuery(((SubSelect) expr.getRightItemsList()).getSelectBody().toString());
                    } catch (JSQLParserException e) {
                        System.out.println("Error parsing sub select!");
                    }
                }
            }

            @Override
            public void visit(IsNullExpression expr) {
                expr.getLeftExpression().accept(new ExpressionVisitorExtended(1));
            }

            @Override
            public void visit(LikeExpression expr) {
                expr.getLeftExpression().accept(new ExpressionVisitorExtended(3));
                expr.getRightExpression().accept(new ExpressionVisitorExtended(3));
            }

            @Override
            public void visit(SubSelect subSelect) {
                try {
                    analyzeQuery(subSelect.getSelectBody().toString());
                } catch (JSQLParserException e) {
                    System.out.println("Error parsing sub select!");
                }
            }

            @Override
            public void visit(ExistsExpression expr) {
                expr.getRightExpression().accept(this);
            }

            @Override
            public void visit(NotExpression notExpr) {
                notExpr.getExpression().accept(this);
            }

            @Override
            protected void visitBinaryExpression(BinaryExpression expr) {
                if(expr instanceof MinorThan || expr instanceof MinorThanEquals || expr instanceof GreaterThan || expr instanceof GreaterThanEquals) {
                    expr.getLeftExpression().accept(new ExpressionVisitorExtended(2));
                    expr.getRightExpression().accept(new ExpressionVisitorExtended(2));
                } else if (expr instanceof EqualsTo || expr instanceof NotEqualsTo || expr instanceof Matches) {
                    expr.getLeftExpression().accept(new ExpressionVisitorExtended(1));
                    expr.getRightExpression().accept(new ExpressionVisitorExtended(1));
                } else if (expr instanceof Addition || expr instanceof Division || expr instanceof Multiplication
                        || expr instanceof Subtraction || expr instanceof IntegerDivision || expr instanceof Modulo) {
                    expr.accept(new ExpressionVisitorExtended(3));
                }
            }

            @Override
            public void visit(SimilarToExpression expr) {
                super.visit(expr);
            }

            @Override
            public void visit(WhenClause expr) {
                expr.getWhenExpression().accept(this);
                expr.getThenExpression().accept(this);
            }
        };
    }

    /**
     * Function that loads queries and analyzes them one by one.
     */
    private void readSQLQueries() {
        try {
            loadQueriesFromFile();
        } catch (IOException e) {
            System.out.println("Error loading queires.");
        }

        int i = 1;
        for (String query: queries) {
            try {
                listOfTables.clear();
                System.out.println("\nAnalyzing query " + i++ + "...");
                analyzeQuery(query);
            } catch (JSQLParserException e) {
                System.out.println("Error parsing");
            }
        }

        printResult();

        printStatistics();
    }

    /**
     * Function that prints results' statistics.
     */
    private void printStatistics() {
        for (int i = 0; i < 5; i++) {
            System.out.print("\n");
            System.out.printf("%40s : ", resultsMeaning[i]);
            System.out.printf("%4d", resultStatistics[i]);
            System.out.printf("%5s", "");
            System.out.printf("%3.2f%%", (double) resultStatistics[i]/requestCounter.keySet().size()*100);
        }
        System.out.printf("\n%40s : ", "Method(s)");
        System.out.printf("%4d", requestCounter.keySet().size());
        System.out.printf("%5s", "");
        System.out.printf("%3.2f%%", (double) 100);
    }

    /**
     * Function that loads queries from file.
     * @throws IOException
     */
    private void loadQueriesFromFile() throws IOException {
        this.queries = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader("src\\main\\resources\\sqlQueries"));
        //BufferedReader br = new BufferedReader(new FileReader("src\\main\\resources\\sqlQueries1"));
        String line;
        String query = "";
        while ((line = br.readLine()) != null) {
            query += line;
            query += " ";
            if (line.endsWith(";")) {
                this.queries.add(query);
                query = "";
            }
        }
    }

    /**
     * Function that analyzes query. Steps of analyzing depend on type of query.
     * @param query String of sql query.
     * @throws JSQLParserException
     */
    private void analyzeQuery(String query) throws JSQLParserException {
        Statement parsedQuery = CCJSqlParserUtil.parse(query);

        if (parsedQuery instanceof Select) {

            ArrayList<Select> selects = new ArrayList<>();
            if (!(((Select) parsedQuery).getSelectBody() instanceof PlainSelect)) {
                for (SelectBody select: ((SetOperationList)((Select) parsedQuery).getSelectBody()).getSelects()) {
                    selects.add((Select) CCJSqlParserUtil.parse(select.toString()));
                }
            } else {
                selects.add((Select) parsedQuery);
            }

            for (Select select: selects) {
                fillTables(select);

                analyzeSelectPartOfQuery(select);

                analyzeWherePartOfQuery(CCJSqlParserUtil.parseCondExpression(String.valueOf(((PlainSelect) (select).getSelectBody()).getWhere())));

                analyzeGroupByPartOfQuery(select);

                analyzeOrderByPartOfQuery(select);

                analyzeHavingPartOfQuery(select);
            }

        } else if (parsedQuery instanceof CreateView) {

            analyzeQuery(((CreateView) parsedQuery).getSelect().toString());

        } else if (parsedQuery instanceof Delete) {

            fillTables(parsedQuery);

            analyzeWherePartOfQuery(((Delete) parsedQuery).getWhere());

        } else if (parsedQuery instanceof Update) {

            fillTables(parsedQuery);

            analyzeWherePartOfQuery(((Update) parsedQuery).getWhere());

        } else {
            System.out.println("Nothing to do");
        }
    }


    /**
     * Function that fills all the tables from from and join part into the array.
     * @param query Query from which we find tables
     */
    private void fillTables(Statement query) {
        String fromTable = null;
        List<Join> joins = null;
        if (query instanceof Select) {
            if (((PlainSelect) ((Select) query).getSelectBody()).getFromItem() instanceof SubSelect) {
                try {
                    analyzeQuery(((SubSelect) (((PlainSelect) ((Select) query).getSelectBody()).getFromItem())).getSelectBody().toString());
                } catch (JSQLParserException e) {
                    System.out.println("Error parsing sub select!");
                }
                return;
            }
            fromTable = ((PlainSelect) ((Select) query).getSelectBody()).getFromItem().toString();
            if (((PlainSelect) ((Select) query).getSelectBody()).getFromItem().getAlias() != null) {
                fromTable = fromTable.substring(0, fromTable.lastIndexOf(((PlainSelect) ((Select) query).getSelectBody()).getFromItem().getAlias().toString()));
            }
            if (((PlainSelect) ((Select) query).getSelectBody()).getJoins() != null) {
                joins = ((PlainSelect) ((Select) query).getSelectBody()).getJoins();
            }
        } else if (query instanceof Delete) {
            fromTable = ((Delete) query).getTable().toString();
            joins = ((Delete) query).getJoins();
        } else if (query instanceof Update) {
            fromTable = ((Update) query).getTable().toString();
            joins = ((Update) query).getJoins();
        }
        if (fromTable != null) listOfTables.add(fromTable);
        if (joins != null) {
            for (Join joinTable : joins) {
                listOfTables.add(((net.sf.jsqlparser.schema.Table) joinTable.getRightItem()).getName());
                if (joinTable.getOnExpression() != null) {
                    joinTable.getOnExpression().accept(conditionVisitor);
                }
            }
        }
    }

    /**
     * Function for analyzing select part of query.
     * @param select Select query
     */
    private void analyzeSelectPartOfQuery(Select select) {

        for (SelectItem selectItem : ((PlainSelect)select.getSelectBody()).getSelectItems()) {

            selectItem.accept(new SelectItemVisitor() {

                @Override
                public void visit(AllColumns allColumns) {
                    markAllColumnsFromTable(select);
                }

                @Override
                public void visit(AllTableColumns allTableColumns) {
                    System.out.println("All tables columns " + allTableColumns.toString());
                    //TODO: sta tocno ovo predstavlja?
                }

                @Override
                public void visit(SelectExpressionItem selectExpressionItem) {
                    selectExpressionItem.getExpression().accept(new ExpressionVisitorExtended(0));
                }
            });

        }
    }

    private void analyzeWherePartOfQuery(Expression where) throws JSQLParserException {
        if (where != null) {
            where.accept(conditionVisitor);
        }
    }

    private void analyzeOrderByPartOfQuery(Select select) {
        //((PlainSelect)select.getSelectBody()).getOrderByElements()
        return;
    }

    private void analyzeGroupByPartOfQuery(Select select) {
        //((PlainSelect)select.getSelectBody()).getGroupBy().groupByExpressions
        return;
    }

    private void analyzeHavingPartOfQuery(Select select) {
        Expression expression = ((PlainSelect)select.getSelectBody()).getHaving();
        if (expression != null) {
            expression.accept(conditionVisitor);
        }
    }

    /**
     * Function that checks if key exists in map.
     * @param key String value of key
     * @return boolean true if it exists, false otherwise
     */
    private boolean checkIfKeyExists(String key) {
        if (requestCounter.containsKey(key)) {
            return true;
        }
        return false;
    }

    /**
     * Function that marks flag for data retrival for all columns in table.
     * @param select
     */
    private void markAllColumnsFromTable(Select select) {
        String table = ((PlainSelect)select.getSelectBody()).getFromItem().toString();
        if (((PlainSelect)select.getSelectBody()).getFromItem().getAlias() != null) {
            table = table.substring(0, table.lastIndexOf(((PlainSelect)select.getSelectBody()).getFromItem().getAlias().toString()));
        }
        for (String key: requestCounter.keySet()) {
            if (key.startsWith(table.toUpperCase() + ".")) {
                incrementValues(key, 0);
            }
        }
    }

    /**
     * Function that finds table of given column and marks flag for data action.
     * @param column name of column
     * @param index id of data action
     */
    private void findColumns(String column, int index) {
        String table = findTable(column);
        if (table == null) {
            return;
        }
        String key = table + "." + column;
        incrementValues(key.toUpperCase(), index);
    }

    /**
     * Function that increments value of data action by one.
     * @param key name of key
     * @param index number that represents data action
     */
    private void incrementValues(String key, int index) {
        int[] values = requestCounter.get(key);
        values[index]++;
        requestCounter.put(key, values);
    }

    /**
     * Function that finds table for certain column.
     * @param column
     * @return Returns table name if found, null otherwise.
     */
    private String findTable(String column) {
        for (String table: listOfTables) {
            if (requestCounter.containsKey((table + "." + column).toUpperCase())) {
                return table;
            }
        }
        return null;
    }

    /**
     * Function that initializes map with counters.
     */
    private void initializeRequestCounter() {
        this.requestCounter = new LinkedHashMap<>();
        for (Table table: this.database.getTables()) {
            for (Column column : table.getColumns()) {
                int[] counter = new int[NUMBER_OF_REQUESTS];
                requestCounter.put(table.getName() + "." + column.getName(), counter);
            }
        }
    }

    /**
     * Function for printing results.
     */
    private void printResult() {
        Iterator iterator = requestCounter.keySet().iterator();
        while (iterator.hasNext()) {
            String name = iterator.next().toString();
            System.out.printf("%30s : ", name);
            for (int i = 0; i < NUMBER_OF_REQUESTS; i++) {
                if (this.requestCounter.get(name)[i] > 0) {
                    System.out.printf(ANSI_GREEN + "%4d " + ANSI_RESET, this.requestCounter.get(name)[i]);
                } else {
                    System.out.printf("%4d ", this.requestCounter.get(name)[i]);
                }
            }
            System.out.printf("%40s", resultsMeaning[determinePossibleMethods(this.requestCounter.get(name))]);
            System.out.print("\n");
        }
    }

    /**
     * Function that determine which method should be used over given data.
     * @param result
     * @return
     */
    private int determinePossibleMethods(int[] result) {
        int resultIndex = 4;
        if (result[3] > 0 || result[2] > 0) {
            resultIndex = 0;
        } else {
            if (result[0] > 0) {
                if (result[1] > 0) {
                    resultIndex = 1;
                } else {
                    resultIndex = 2;
                }
            } else {
                if (result[1] > 0) {
                    resultIndex = 3;
                }
            }
        }
        resultStatistics[resultIndex]++;
        return resultIndex;
    }

    /**
     * Class that represents Expression Visitor.
     */
    private class ExpressionVisitorExtended extends ExpressionVisitorAdapter {

        /**
         * Index represents how the expression is used in sql query.
         */
        private int index;

        public ExpressionVisitorExtended(int index) {
            this.index = index;
        }

        @Override
        public void visit(Function function) {
            if (function.getParameters() != null) {
                for (Expression e: function.getParameters().getExpressions()) {
                    this.index = 3;
                    e.accept(this);
                }
            } else if (function.getNamedParameters() != null) {
                for (Expression e: function.getNamedParameters().getExpressions()) {
                    this.index = 3;
                    e.accept(this);
                }
            }
        }

        @Override
        public void visit(Parenthesis parenthesis) {
            parenthesis.getExpression().accept(this);
        }

        @Override
        public void visit(net.sf.jsqlparser.schema.Column column) {
            if (column.getTable() != null) {
                String key = column.getFullyQualifiedName().toUpperCase();
                if (checkIfKeyExists(key)) {
                    incrementValues(key, this.index);
                } else {
                    findColumns(column.getColumnName(), this.index);
                }
            } else {
                findColumns(column.getColumnName(), this.index);
            }
        }

        @Override
        public void visit(SubSelect subSelect) {
            try {
                analyzeQuery(subSelect.getSelectBody().toString());
            } catch (JSQLParserException e) {
                System.out.println("Error parsing sub select!");
            }
        }

        @Override
        public void visit(CaseExpression expr) {
            for (WhenClause clause: expr.getWhenClauses()) {
                clause.accept(conditionVisitor);
            }
            expr.getElseExpression().accept(conditionVisitor);
        }

        @Override
        public void visit(ExtractExpression expr) {
            this.index = 3;
            expr.getExpression().accept(this);
        }

        @Override
        protected void visitBinaryExpression(BinaryExpression expr) {
            if (expr instanceof Addition || expr instanceof Division || expr instanceof Multiplication
                    || expr instanceof Subtraction || expr instanceof IntegerDivision || expr instanceof Modulo) {
                this.index = 3;
                expr.getLeftExpression().accept(this);
                expr.getRightExpression().accept(this);
            }
        }
    }

}

