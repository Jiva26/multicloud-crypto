package hr.fer.zemris.input;

import java.util.ArrayList;
import java.util.List;

public class Table {

    private String name;
    private List<Column> columns;

    public Table(String name) {
        this.name = name;
        this.columns = new ArrayList<Column>();
    }

    public String getName() {
        return name;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void addColumn(Column column) {
        this.columns.add(column);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name + ": ");
        for (Column column: this.columns) {
            if (this.columns.size() - 1 == this.columns.indexOf(column)) {
                stringBuilder.append(column.getName() + "\n");
            } else {
                stringBuilder.append(column.getName() + ", ");
            }
        }
        return stringBuilder.toString();
    }
}
