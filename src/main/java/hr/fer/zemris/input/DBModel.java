package hr.fer.zemris.input;

import java.util.ArrayList;
import java.util.List;

public class DBModel {

    private List<Table> tables;

    public DBModel() {
        this.tables = new ArrayList<Table>();
    }

    public List<Table> getTables() {
        return tables;
    }

    public void addTable(Table table) {
        this.tables.add(table);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Table table: this.tables) {
            stringBuilder.append(table.toString());
        }
        return stringBuilder.toString();
    }
}
